<?php
/**
 * Created by PhpStorm.
 * User: marco
 * Date: 14/09/15
 * Time: 11.19
 * To change this template use File | Settings | File Templates.
 */
class Bitbull_CategoryNavigation_Model_System_Config_Source_Categories {

    public function toOptionArray()
    {
        $options = array();

        $collection =  Mage::getResourceModel('catalog/category_collection')
            ->addAttributeToSelect('name')
            ->addFieldToFilter('level', array('neq' => 0))
            ->setOrder('name')
//            ->setOrder('level', 'DESC')
            ;

        foreach ($collection as $category) {
            /** @var $category Mage_Catalog_Model_Category */
            $options[] = array(
                    'label' => $category->getName() .' ('.$category->getId().')',
                    'value' => $category->getId()
                );
            }

        return $options;
    }
}