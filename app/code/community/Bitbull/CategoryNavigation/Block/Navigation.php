<?php

class Bitbull_CategoryNavigation_Block_Navigation extends Mage_Core_Block_Template {
	protected $_mainCategory=null;
	protected $_categories=null;
	protected $_currentCategoryId=null;
	const XML_PATH_CATEGORIES = 'bitbull_categorynavigation/settings/categories';


	public function getMainCategory() {
		if($this->_categories === null) {
			$this->loadCategories();
		}
		return $this->_mainCategory;
	}
	public function getCategories() {
		if($this->_categories === null) {
			$this->loadCategories();
		}
		return $this->_categories;
	}
	public function isCurrentCategory($category) {
		return $this->_currentCategoryId==$category->getEntityId();
	}

	protected function loadCategories() {
		 $config = Mage::getModel('bitbull_categorynavigation/config');
		 if(!$config->getEnabled()) {
			 return array();
		 }

		 /** @var Mage_Catalog_Model_Category $category */
		 // Get current category
		 $category = Mage::registry('current_category');
		 if(!$category) {
			 return array();
		 }

		 // Check level
		 if($category->getLevel()<1) {
			 return array();
		 }

		 $this->_currentCategoryId = $category->getEntityId();

		 // Find base category
		 $baseCategoryId = $category->getEntityId();
		 $count = Mage::getModel('catalog/category') -> getCollection()
			 -> addAttributeToFilter('is_active', array('gt' => 0))
			 -> addAttributeToFilter('parent_id', array('eq' => $baseCategoryId))
			 -> count();

		 //recupero i parent delle categorie che non devono visualizzare i fratelli
		 $categoriesToExclude = explode(',',Mage::getStoreConfig(self::XML_PATH_CATEGORIES));
		 if($count==0 && array_search($category->getParentId(),$categoriesToExclude)===false) {
			 $baseCategoryId = $category->getParentId();
			 $this->_mainCategory = Mage::getModel('catalog/category')
				 -> load($baseCategoryId);
		 } else {
			 $this->_mainCategory = $category;
		 }

		 // Categories
		 $this->_categories = Mage::getModel('catalog/category') -> getCollection()
			 -> addAttributeToSelect('name')
			 -> addAttributeToFilter('is_active', array('gt' => 0))
			 -> addAttributeToFilter('parent_id', array('eq' => $baseCategoryId))
			 -> addAttributeToSort('position');

	 }
}
